/**
 * is called when the Form for new Items is submitted
 * @returns {boolean}
 */
function btnEnquiry() {

    var valName        = $('#ajaxexample_name').val();
    var valDescription = $('#ajaxexample_description').val();

    $.ajax({
        async: 'true',
        url: 'http://typo3:8888/',
        type: 'POST',

        data: {
            eID: "ajaxDispatcher",
            request: {
                pluginName:  'ajaxexample',
                controller:  'Item',
                action:      'ajax',
                arguments: {
                    'name': valName,
                    'description': valDescription
                }
            }
        },
        dataType: "json",
        success : success,
        //success: function(result) {
        //    console.log('succsess: ' , result);
        //},
        error: function(error) {
            console.log('error: ' , error);
        }
    });
    return false;
}

/**
 * success form ajax call
 * @param result
 */
function success (result) {
    console.log('succsess2: ' , result);
    setFormResultView(result);
}

/**
 * shows the result from the request
 * @param result
 */
function setFormResultView(result){
    $('#resp_name').text(result.name);
    $('#resp_description').text(result.description);
    $('#flash_message').text(result.message);
    $('#enquiry_form').hide();
    $('#enquiry_response').show();
}

/**
 * Shows the Form
 */
function setFormView(){
    $('#resp_name').text('');
    $('#resp_description').text('');
    $('#enquiry_form').show();
    $('#enquiry_response').hide();
}

/**
 * is called when the more button is clicked
 */
function btnMoreEnquiry() {
    setFormView();
}