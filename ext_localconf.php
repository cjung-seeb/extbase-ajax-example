<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$TYPO3_CONF_VARS['FE']['eID_include']['ajaxDispatcher'] = t3lib_extMgm::extPath('ajaxexample').'Classes/Utility/Dispatcher.php';
//$TYPO3_CONF_VARS['FE']['eID_include']['ajaxDispatcher'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('projects_and_tasks').'Classes/EidDispatcher.php';

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TYPO3.' . $_EXTKEY,
	'Ajaxexample',
	array(
		'Item' => 'list, show, new, create, edit, update, delete, ajax',
		
	),
	// non-cacheable actions
	array(
		'Item' => 'list, create, update, delete',
		
	)
);

?>